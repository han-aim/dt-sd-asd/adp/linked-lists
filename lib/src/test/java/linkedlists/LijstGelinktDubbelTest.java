package linkedlists;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Unit tests voor eigen implementatie van dubbel gelinkte lijst.
 */
public class LijstGelinktDubbelTest {
  /**
   * Fixture voor lege linked list.
   */
  private static final LijstGelinktDubbel<String> ITEM_NULDE =
      new LijstGelinktDubbel<>("Nulde", null, null);
  /**
   * Fixture voor lege linked list.
   */
  private static final LijstGelinktDubbel<String> ITEM_TWEEDE =
      new LijstGelinktDubbel<>("Tweede", null, null);

  @Test
  public void voegToeVerwijder() {
    final LijstGelinktDubbel<String> lijstgelinktdubbel =
        new LijstGelinktDubbel<>("Eerste", null, null);
    lijstgelinktdubbel.setItemVorige(ITEM_NULDE);
    lijstgelinktdubbel.setItemVolgende(ITEM_TWEEDE);
    assertEquals(lijstgelinktdubbel.getItemVorige(), ITEM_NULDE);
    assertEquals(lijstgelinktdubbel.getItemVolgende(), ITEM_TWEEDE);
  }
}

package linkedlists;

import static org.junit.jupiter.api.Assertions.assertAll;

import java.util.LinkedList;
import org.junit.jupiter.api.Test;

/**
 * Unit tests voor standaardbibliotheek-implementatie van dubbel gelinkte lijst.
 */
public class LinkedListTest {
  private static final String ITEM = "Iets";

  @Test
  public void voegaanLos() {
    assertAll(() -> {
      final LinkedList<String> list = new LinkedList<>();
      list.add(ITEM);
      list.add(ITEM);
      list.add(ITEM);
    });
  }

  @Test
  public void voegaanVerzameling() {
    assertAll(() -> {
      final LinkedList<String> list = new LinkedList<>();
      final LinkedList<String> list2 = new LinkedList<>();
      list2.add(ITEM);
      list2.add(ITEM);
      list2.add(ITEM);
      list.addAll(list2);
    });
  }
}

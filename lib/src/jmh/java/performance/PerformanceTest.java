package performance;

import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.*;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.NANOSECONDS)
@Measurement(iterations = 10, time = 1, timeUnit = TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
public class PerformanceTest {

  @Param({ "1", "100", "1000" })
  public int n;

  @Benchmark
  public int fragment_1() {
    int sum = 0;
    for (int i = 0; i < n; i++) {
      sum++;
    }
    return sum;
  }

  @Benchmark
  public int fragment_2() {
    int sum = 0;
    for (int i = 0; i < n; i += 2) {
      sum++;
    }
    return sum;
  }

  @Benchmark
  public int fragment_3() {
    int sum = 0;
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        sum++;
      }
    }
    ArrayList<Integer> dynrij = new ArrayList<>();
    return sum;
  }

  @Benchmark
  public int fragment_4() {
    int sum = 0;
    for (int i = 0; i < n; i++) {
      sum++;
    }
    for (int j = 0; j < n; j++) {
      sum++;
    }
    return sum;
  }

  @Benchmark
  public int fragment_5() {
    int sum = 0;
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n * n; j++) {
        sum++;
      }
    }
    return sum;
  }

  @Benchmark
  public int fragment_6() {
    int sum = 0;
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < i; j++) {
        sum++;
      }
    }
    return sum;
  }

  @Benchmark
  public int fragment_7() {
    int sum = 0;
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n * n; j++) {
        for (int k = 0; k < j; k++) {
          sum++;
        }
      }
    }
    return sum;
  }

  @Benchmark
  public int fragment_8() {
    int sum = 0;
    for (int i = 1; i < n; i = i * 2) {
      sum++;
    }
    return sum;
  }

  public static void main(String[] args) throws Exception {
    org.openjdk.jmh.Main.main(args);
  }
}

package linkedlists;

import lombok.Data;

/**
 * Dubbel gelinkte lijst.
 */
@Data
public class LijstGelinktDubbel<Item> {
  private Item item;
  private LijstGelinktDubbel<Item> itemVolgende;
  private LijstGelinktDubbel<Item> itemVorige;

  LijstGelinktDubbel(Item item, LijstGelinktDubbel<Item> itemVorige,
      LijstGelinktDubbel<Item> itemVolgende) {
    this(itemVorige, itemVolgende);
    this.item = item;
  }

  LijstGelinktDubbel(LijstGelinktDubbel<Item> itemVorige, LijstGelinktDubbel<Item> itemVolgende) {
    this.itemVorige = itemVorige;
    this.itemVolgende = itemVolgende;
  }
}

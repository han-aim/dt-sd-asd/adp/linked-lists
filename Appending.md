Appending linked lists

<!-- TODO: Een lastigheid hierbij is dat de method addAll heet i.p.v. append. een verwijzing naar alle elementen. Een verwijzing naar list2 kan niet, want als list2 dan veranderd wordt moet list ook plots veranderen (mutability). Een verwijzing naar list2 zou wel kunnen als je de variabele list2 niet meer toegankelijk maakt. Dan heet het geen append maar concatenate destructively. Met Java Streams kan je een iterator maken over beide.
https://www.baeldung.com/java-init-list-one-line#factory-methods-java-9
http://java-performance.info/linkedlist-performance/
https://docs.oracle.com/en/java/javase/14/docs/api/java.base/java/util/LinkedList.html#addAll(java.util.Collection)
-->
